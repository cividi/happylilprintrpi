# coding: utf-8

# Receipt Web service

from flask import Flask, Markup
from flask import (
    url_for, jsonify,
    request, redirect,
    render_template,
    send_from_directory,
)

import openrouteservice as ors
import numpy
import math
import random
import datetime
import json
import folium
import sqlalchemy
import time

import os
from dotenv import load_dotenv
load_dotenv()

import geopandas as gp
import pylab as plt
import matplotlib
matplotlib.use('Agg')

from shapely.geometry import mapping, shape
from shapely.geometry import Point, Polygon, LineString

from lib.geohelpers import *

# Check the environment
if not set(["API_KEY_ORS", "DATABASE_URL"]) <= set(os.environ):
   print("Environment variables needed (as .env file) to run this script:")
   print("export API_KEY_ORS=...")
   print("export DATABASE_URL=...")
   exit()

# Create a web app
app = Flask(__name__, static_url_path='/static')

# Connect to geocoding service
clnt = ors.client.Client(key=os.environ["API_KEY_ORS"])

# Data connection
postgresDB = os.environ["DATABASE_URL"]
engine = sqlalchemy.create_engine(postgresDB)
con = engine.connect()

# Single user
dta = None
start_time = None

poisList = None
veloisochrones = None
veloisochrones10 = None

@app.route("/")
def hello():
    return app.send_static_file('index.html')

@app.route("/s/<path:path>")
def snow(path):
    return send_from_directory('static', path)

@app.route("/c", methods=['GET'])
def world():
    global dta
    global start_time

    global poisList
    global veloisochrones
    global veloisochrones10

    q = request.args.get('q')
    jsonfmt = request.args.get('json')
    if not q or len(q.strip()) < 3: return {}
    if jsonfmt: jsonfmt = bool(jsonfmt)

    # Simple caching
    if start_time is None: start_time = time.time()
    elapsed_time = time.time() - start_time
    if dta is not None and dta['q'] == q and elapsed_time < 60:
        return again(jsonfmt)
    start_time = time.time()
    print(q)

    # Configure parameters of search
    startAddress = q
    rangeInMin = 4

    SHOW_ADDRESS = False
    SHOW_CONTEST = False
    currentAvgRadiusInM = 3000
    currentMaxRadiusInM = 3000

    SHOW_AIRPORTS = False
    SHOW_STATIONS = False

    # With that set go on to construct the geometry needed for printout.
    addressCoordinates = geocodeAddress(clnt, startAddress)
    addressPoint = Point(addressCoordinates)
    print("xy:", addressPoint.x, addressPoint.y)

    # Randomness

    query = {'request': 'pois',
            'geojson': dict(type="Point",
                     coordinates=[addressPoint.x, addressPoint.y]),
            'filter_category_ids': [ # max 5
                169, # internet cafe
                448, # coffee shop
                564, # cafe
                365, # coworking space
                272, # garden
                # 528, # vending machine
            ],
            'limit': 80, 'buffer': 400,
            # 'sortby': 'distance'
            }
    if poisList is None:
        poisList = clnt.places(**query)['features']
    print(poisList)
    print("# POIs:", len(poisList))
    myPoi = random.choice(poisList)
    poiDistInMeters = str(round(myPoi['properties']['distance']))
    poiLatLong = str(myPoi['geometry']['coordinates'][1])+','+str(myPoi['geometry']['coordinates'][0])

    # Generate isos

    if veloisochrones is None:
        veloisochrones = isochrones(clnt, addressCoordinates, rangeInMin=rangeInMin)
    if veloisochrones10 is None:
        veloisochrones10 = isochrones(clnt, addressCoordinates, rangeInMin=rangeInMin/2)
    # veloisochrones20 = isochrones(clnt, addressCoordinates, rangeInMin=20)

    addressMarker = generateCircleAroundPointWithRadius(addressPoint, 100)

    velogdf = gp.GeoDataFrame.from_features(veloisochrones)
    velo = reprojectgdf(velogdf)
    velogdf10 = gp.GeoDataFrame.from_features(veloisochrones10)
    velo10 = reprojectgdf(velogdf10)
    # velogdf20 = gp.GeoDataFrame.from_features(veloisochrones20)
    # velo20 = reprojectgdf(velogdf20)

    circle = generateCircleAroundPointWithRadius(addressPoint, currentMaxRadiusInM)

    # Ausdehnung

    gdf = gp.GeoDataFrame(gp.GeoSeries(addressPoint))
    gdf.rename(columns={0:'geometry'},inplace=True)
    gdf = reprojectgdf(gdf)
    maxAusdehnung = calcMaxAusdehnung(gdf,velo)

    # Do some plotting

    fig, ax = plt.subplots()
    addressMarker.plot(ax=ax,color='', edgecolor='black',linestyle='--')
    velo.plot(ax=ax,color='', edgecolor='black')
    velo10.plot(ax=ax,color='', edgecolor='black',linestyle='--').axis('off')
    #velo20.plot(ax=ax,color='', edgecolor='grey',linestyle='--').axis('off')
    # circle.plot(ax=ax,color='', edgecolor='black',linestyle='--').axis('off')

    if SHOW_AIRPORTS:
        try:
            sql = 'SELECT geom, name, code, ST_Distance(geom, ST_SetSRID(ST_GeomFromText(\''+addressPoint.wkt+'\'),4326)) as distance FROM "smartuse"."world-airports" WHERE type=\'Airports\' AND carriers>3 AND name IS NOT Null ORDER BY distance ASC LIMIT 1'
            df = gp.GeoDataFrame.from_postgis(sql, con, geom_col='geom')
            airport = reprojectgdf(gp.GeoSeries([df["geom"][0]]))
            airportMarker = markerAtPoint(airport,1000)

            if df["distance"][0] < 0.2:
                airportMarker.plot(ax=ax,color='black', edgecolor='black')
                ax.text(airport.x + 1000 , airport.y - 400, df["code"][0])
        except Exception as ex:
            print(sql, ex)

    if SHOW_STATIONS:
        try:
            sql = 'SELECT geom, name, ST_Distance(geom, ST_SetSRID(ST_GeomFromText(\''+addressPoint.wkt+'\'),4326)) as distance FROM "smartuse"."world-trainstations" ORDER BY distance ASC LIMIT 1'
            df_stations = gp.GeoDataFrame.from_postgis(sql, con, geom_col='geom')
            station = reprojectgdf(gp.GeoSeries([df_stations["geom"][0]]))
            stationMarker = markerAtPoint(station,750)

            if df_stations["distance"][0] < 0.2:
                stationMarker.plot(ax=ax,color='black', edgecolor='black')
                ax.text(station.x + 750 , station.y - 200, df_stations["name"][0])
        except Exception as ex:
            print(sql, ex)

    if myPoi:
        try:
            poiPoint = Point(myPoi['geometry']['coordinates'])
            poiMarker = generateCircleAroundPointWithRadius(poiPoint, 25)
            # poiMarker = markerAtPoint(,10)
            poiMarker.plot(ax=ax,color='black', edgecolor='black')
        except Exception as ex:
            print(sql, ex)

    # Collect output

    out = []

    if SHOW_ADDRESS:
        out.append(str(rangeInMin)+" Velo-Minuten von")
        for s in startAddress.split(','):
            out.append(s.strip())
        out.append(' ')

    out.append("~~~~~~M~I~S~S~I~O~N~~~~~~")

    if myPoi is not None:
        out.append(' ')
        if 'osm_tags' in myPoi['properties'] and \
            'name' in myPoi['properties']['osm_tags']:
            out.append('  ' + myPoi['properties']['osm_tags']['name'])
            out.append(' ')
        for c in myPoi['properties']['category_ids'].keys():
            out.append('= '+myPoi['properties']['category_ids'][c]['category_name'].replace('_',' ')+' @ '+poiDistInMeters+'m')
        out.append('OSM ID # '+str(myPoi['properties']['osm_id']))
        out.append(poiLatLong)

    if SHOW_CONTEST:
        out.append("Meine Reichweite > "+strMasKm(maxAusdehnung)+"km")
        out.append("Durchschnitt. > " +
            strMasKm(currentAvgRadiusInM)+"km")
            # " / Max " + strMasKm(currentMaxRadiusInM)+"km")

    out.append(" ")
    out.append("(1) Los! Finde den Ort")
    out.append(" ")
    out.append("(2) Beobachte, notiere:")
    out.append("[ ] ÖV Haltestelle")
    out.append("[ ] Fahrradabstellplatz")
    out.append("[ ] Autoparkplatz/Garage")
    out.append("[ ] Überwachungskameras")
    out.append("[ ] Nutzbare Grünfläche")
    out.append("[ ] Öffen. Ein-/Durchgang")
    out.append("[ ] Baustelle, Raumpläne")
    out.append(" ")
    out.append("(3) Teile @")
    out.append("      cividi.ch/post")
    out.append("~~~~~~~~~~~~~~~~~~~~~~~~")
    plt.savefig('static/output/happy.png', bbox_inches='tight')
    plt.close()

    dta = {
        'q':   q,
        'img': '/static/output/happy.png?st='+str(start_time),
        'txt': out
    }
    return again(jsonfmt)

@app.route("/g/<int:jsonfmt>")
def again(jsonfmt):
    # if dta is None:
    #     return redirect(url_for('hello'))
    if jsonfmt and bool(jsonfmt):
        return jsonify(dta)
    return "<style>img{max-width:100%}</style>" \
    + "<img src=\"/s/pushbutton.jpg\"><hr>" \
    + "<tt><img src=\"%s\"><br>%s" % (
        dta['img'], "<br>".join(dta['txt']).replace(' ', '&nbsp;')
    )
