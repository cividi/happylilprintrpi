# Happy Lil' Printr Pi

Tangible urban data experiments inspired by Berg London's [Little Printer](https://www.stylus.com/qfdjpv), based on Adafruit's [Pi Thermal Printer](https://learn.adafruit.com/pi-thermal-printer), connected to web services that generate text and imagery on demand based on geographic coordinates and geodata. Background details can be found in our [Criterion blog post](https://cividi.be/blog/en/2019-04-26).

The Pi server (`main.py`), Pi script (`output.py`), and Flask web app server (`app.py`) code components of our solution are in this repository. The `notebooks` folder contains Jupyter Notebooks that were used in development.

Note that currently this project does not support multiple simultaneous devices in operation.

> _In the future, everything will be connected to the internet. And all restaurants will be Taco Bell!_
<small>-- [Adafruit](https://learn.adafruit.com/pi-thermal-printer/overview)</small>

## Client deployment

On the computer connected to a printer, in our case a Raspberry Pi:

1) Install pip and pipenv

    sudo apt install python3-pip python3-dev
    pip3 install --user pipenv

2) Add pipenv (and other python scripts) to PATH

    echo "PATH=$HOME/.local/bin:$PATH" >> ~/.bashrc
    source ~/.bashrc

3) Set up variables in a `.env` file

      APP_BASE_URL=...
      API_KEY_ORS=...
      DATABASE_URL=...
      FLASK_DEBUG=1 # optional

4) Run the client script

    python main.py


## Server deployment

Repeat steps 1, 2 and 3 on the server.

4a) Run the server application in development

    flask run

4b) To customize the port and enable external access:

    flask run -h 0.0.0.0 -p 4321

(^ preferrably use Gunicorn or another production service for this)
