import openrouteservice as ors
import geopandas as gp


def geocodeAddress(clnt, addressString):
    request = {'text': addressString}
    # try:
    answer = ors.geocode.pelias_search(clnt,**request)
    geocoded = answer["features"][0]["geometry"]["coordinates"]
    # except:
    #     geocoded = ""
    return geocoded

def isochrones(clnt, centerpoint, rangeInMin=30, type="cycling-regular"):
    request = {
        'locations':[centerpoint],
        'range': [rangeInMin*60],
        'profile': type
              }
    # try:
    answer = clnt.isochrones(**request)
    isochrones = answer
    # except:
    #     isochrones = ""
    return isochrones

def generateCircleAroundPointWithRadius(centerPoint, radiusInM):
    centerPoint = reprojectgdf(gp.GeoSeries([centerPoint]))
    circle = centerPoint.buffer(radiusInM)
    circle_gdf = gp.GeoDataFrame(circle)
    circle_gdf.rename(columns={0:'geometry'},inplace=True)

    return circle_gdf

def markerAtPoint(centerPoint, sizeInM):
    offset = sizeInM / 2

    line_horizontal = gp.GeoSeries(LineString([(centerPoint.x-offset,centerPoint.y),(centerPoint.x+offset,centerPoint.y)]))
    line_vertical = gp.GeoSeries(LineString([(centerPoint.x,centerPoint.y+offset),(centerPoint.x,centerPoint.y-offset)]))

    gdf = gp.GeoDataFrame([line_horizontal, line_vertical])
    gdf.rename(columns={0:'geometry'},inplace=True)

    return gdf

def reprojectgdf(gdf, fromCRS="4326", toCRS="2056"):
    gdf.crs = {'init' :'epsg:'+fromCRS}
    return gdf.to_crs({'init': 'epsg:'+toCRS})

def calcMaxAusdehnung(pt, geom):
    ptB = pt.bounds.values[0].tolist()
    ptX = ptB[0]
    ptY = ptB[1]

    geomB = geom["geometry"].bounds.values[0].tolist()

    return max((ptX-geomB[0],ptY-geomB[1],geomB[2]-ptX,geomB[3]-ptY))

def strMasKm(value):
    return str(round(value/1000,2))
