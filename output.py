# coding: utf-8

# Receipt Printer client


from __future__ import print_function
# import base64, HTMLParser, httplib, json, sys, urllib, zlib
from unidecode import unidecode
from PIL import Image, ImageDraw

import requests
import shutil
import os

from lib.Adafruit_Thermal import *

from dotenv import load_dotenv
load_dotenv()


if not set(["APP_BASE_URL"]) <= set(os.environ):
   print("Environment variables needed (as .env file) to run this script:")
   print("export APP_BASE_URL=...")
   exit()

BASE_URL = os.environ['APP_BASE_URL']

URL = BASE_URL + "/g/1"

printer   = Adafruit_Thermal("/dev/serial0", 19200, timeout=5)

r = requests.get(URL)
j = r.json()

if j is None: exit()

imgpath = BASE_URL + j['img']
imgfile = "static/output/temp.png"

r = requests.get(imgpath, stream=True)
if r.status_code == 200:
    with open(imgfile, 'wb') as f:
        for chunk in r:
            f.write(chunk)

img = Image.open(imgfile)
#img = img.crop([70,1,464,394])


printer.inverseOn()
printer.print('      cividi      ')
printer.inverseOff()
printer.feed(1)
printer.print('  #CivicUrbanism')
printer.feed(1)
printer.printImage(img, True)

for d in j['txt']:
  printer.print(unidecode(d))
  printer.feed(1)

printer.feed(3)
